const jwt = require(`jsonwebtoken`);
const bcrypt = require(`bcrypt`);
const express = require('express');
const User = require('../server/model/userSchema');
const router = express.Router();

router.get('',(req,res)=>{
    res.send(`Helow world from the server routere js`);
});
router.post (`/register`,async(req,res)=>{

   const {name,email,phone,work,password,cpassword} = req.body;
   if(!name || !email || !phone || !work || !password || !cpassword){
       return res.status(422).json({error:"plz fillede the requirment property"});
   }
   try{
       const userExist = await User.findOne({email:email});
       if(userExist){
           return res.status(422).json({error:"email is already exist"});
       }
    //    }else if (password != cpassword){
    //        return res.status(422).json({error:"password are not Match"});
    //    }else{
        const user =new User({name,email,phone,work,password,cpassword});

        await user.save();
        res.status(201).json({message:"user registered sucessfully"});
       
} catch(err) {
    console.log(err);
 }
});

// login route 
router.post(`/signin`,async(req,res)=>{
    try{
        let token;
        const {email,password}= req.body;
        if(!email || !password){
            return res.status(400).json({error:"plz filleld the data"})
        }
        const userLogin = await User.findOne({email:email});
        // console.log(userLogin);
        if(userLogin){
            // res.status(400).json({error:"user error"});
            const isMatch =await bcrypt.compare(password,userLogin.password);
            token = await userLogin.generateAuthToken();
            console.log(token);
            res.cookie("jwtoken", token,{
                expires: new Date(Date.now()+25892000000),
                httpOnly:true
            })
            if(!isMatch){
                res.status(400).json({error:"invalid Credential"});
            }else {
                res.json({message:"user Signin Sucessful"});
            }
            }else {
                res.status(400).json({error :"invalid  Credentials"});
            }
    } catch(err){
        console.log(err);
    }
});


module.exports = router;