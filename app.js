const dotenv =require("dotenv");
const mongoose = require(`mongoose`);
const express = require('express');
const app= express();

const bodyParser = require("body-parser");
const middleware = (req,res,next)=>{
    console.log(`helow my Middleware`);
    next();
};
app.use(express.json());
app.use(require(`./router/auth`));
const User = require('./server/model/userSchema');

dotenv.config({path:'./config.env'});
require('./conn');


app.get('/',(req,res)=>{
    res.send(`hello world from the server`);
});
app.get('/about',middleware,(req,res)=>{
    console.log(`hello my About`);
    res.send(`hello world from the about page server`);

});
app.get('/contact',(req,res)=>{
    res.send(`hello contact world from the about page server`);
});
app.listen(5000,()=>{
    console.log(`server is running at the port no 5000`);
});
